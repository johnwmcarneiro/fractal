# README

## Requisitos
- Ruby 2.3.4
- Rails 5.1
- PostgreSQL >= 9.5
- ElasticSearch

## Instalação
Para instalar a aplicação, abra seu terminal e navegue até a pasta do projeto (```cd diretorio_do_projeto```), depois execute os comandos abaixo.
Lembrando que é necessário já ter o Ruby e Rails instalado.
```
bundle install
rake db:create
rake db:migrate
rake db:seed # load sample contents
```

## Como usar
Para realizar as operações via Postman é necessário levantar o servidor.
```
bundle exec bin/rails server -p 3000
```

## Postman
Para utilizar via postman, é necessário informar as seguintes variáveis no Header.
```
X-User-Email: EMAIL_DO_USUARIO
X-User-Token: TOKEN_DO_USUARIO
```
Para conseguir essas informações basta que acesse o console do Rails e busque o primeiro usuário.
```
rails console
user = User.first
# <User:0X... email: test@example.com, ..., authentication_token: 'TOKEN_DO_USUARIO'>
```

## Testes
Para executar a suíte de testes para execute o seguinte comando no terminal.
```
bundle exec rspec
```

## Documentação
Para acessar a documentação basta que inicie a aplicação com ```rails server``` e acesse no navegador a url http://localhost:3000/docs