Rails.application.routes.draw do
  devise_for :users

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :people
      post 'sessions', to: 'sessions#create'
    end
  end

  mount Raddocs::App => '/docs'
end
