module Api
  module V1
    class PeopleController < ApiController
      before_action :set_person, only: [:show, :edit, :update, :destroy]

      def index
        per_page = 30

        if params[:q].present?
          @people = Person.search(params[:q],
                                  fields: [:name, :company, :title,
                                           :categories, :contact_data],
                                  where: { user_id: current_user.id },
                                  aggs: { name: { order: { '_term' => 'ASC' } } },
                                  per_page: per_page,
                                  page: params[:page])
        else
          @people = current_user.people
                                .order(:name)
                                .paginate(per_page: per_page,
                                          page: params[:page])
        end

        render json: @people,
               each_serializer: PersonSerializer,
               meta: merge_attrs(@people),
               root: 'people'
      end

      def show
        render json: @person
      end

      def create
        @person = Person.create! person_params.merge(user: current_user)
        render json: @person, status: :created
      end

      def update
        @person.update person_params
        render json: @person
      end

      def destroy
        @person.destroy
      end

      private

      def set_person
        @person = current_user.people.find params[:id]
      end

      def person_params
        params.permit(:name, :company, :title, :born_at, category_ids: [],
                      contact_data_attributes: [:id, :data_type, :value, :_destroy],
                      addresses_attributes: [:id, :street, :number, :complement,
                                             :neighborhood, :city, :state,
                                             :zipcode, :_destroy])
      end
    end
  end
end
