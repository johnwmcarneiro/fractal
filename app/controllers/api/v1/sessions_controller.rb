module Api
  module V1
    class SessionsController < ApplicationController
      def create
        user = User.find_for_database_authentication(email: params[:email])
        return invalid_login_attempt unless user

        if user.valid_password? params[:password]
          sign_in :user, user
          render json: { email: user.email,
                         token: user.authentication_token}
          return
        end

        invalid_login_attempt
      end

      private

      def invalid_login_attempt
        render json: { message: 'Invalid email or password' }, status: 401
      end
    end
  end
end
