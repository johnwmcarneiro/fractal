module Api
  module V1
    class ApiController < ApplicationController
      acts_as_token_authentication_handler_for User
      before_action :require_authentication!

      def merge_attrs(collection, extra_meta = {})
        {
          current_page: collection.current_page,
          next_page: collection.next_page,
          prev_page: collection.previous_page,
          total_pages: collection.total_pages,
          total_entries: collection.total_entries
        }.merge(extra_meta)
      end

      private

      def require_authentication!
        throw(:warden, scope: :user) unless current_user.presence
      end
    end
  end
end
