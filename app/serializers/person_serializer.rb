class PersonSerializer < ActiveModel::Serializer
  attributes :id, :name, :company, :title, :born_at, :created_at
  attributes :categories

  has_many :addresses
  has_many :contact_data

  def categories
    object.categories.map { |category| CategorySerializer.new(category) }
  end
end
