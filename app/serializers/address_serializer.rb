class AddressSerializer < ActiveModel::Serializer
  attributes :id, :street, :number, :complement, :neighborhood,
             :city, :state, :zipcode

  belongs_to :person
end
