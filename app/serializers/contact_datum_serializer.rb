class ContactDatumSerializer < ActiveModel::Serializer
  attributes :id, :data_type, :value

  belongs_to :person
end
