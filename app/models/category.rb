class Category < ApplicationRecord
  # Associations
  has_and_belongs_to_many :people

  # Validations
  validates :name, presence: true

  def self.init_categories
    options = %w[customer vendor shipping_company partner employee owner]
    options.each_with_index do |c, i|
      id = i + 1
      create(id: id, name: c) unless exists? name: c
    end
  end
end
