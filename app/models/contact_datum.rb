class ContactDatum < ApplicationRecord
  # Enuns
  enum data_type: [:phone, :email, :site]

  # Associations
  belongs_to :person

  # Validations
  validates :person, presence: true
  validates :data_type, presence: true
  validates :value, presence: true
end
