class Person < ApplicationRecord
  default_scope { includes(:categories, :contact_data, :addresses) }

  # Associations
  belongs_to :user
  has_and_belongs_to_many :categories
  has_many :contact_data, dependent: :destroy
  accepts_nested_attributes_for :contact_data, allow_destroy: true
  has_many :addresses, dependent: :destroy
  accepts_nested_attributes_for :addresses, allow_destroy: true

  # Validations
  validates :user_id, presence: true
  validates :user, presence: true
  validates :name, presence: true

  # Macros
  searchkick

  def search_data
    {
      name: name,
      company: company,
      title: title,
      user_id: user_id,
      categories: categories.map(&:name).join(' ').to_s,
      contact_data: contact_data.map(&:value).join(' ').to_s
    }
  end
end
