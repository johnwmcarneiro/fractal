class Address < ApplicationRecord
  # Associations
  belongs_to :person

  # Validations
  validates :person, presence: true
  validates :street, presence: true
end
