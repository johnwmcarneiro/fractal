require 'rails_helper'

RSpec.describe ContactDatum, type: :model do
  context 'Associations' do
    it { should belong_to(:person) }
  end

  context 'Validations' do
    it { should validate_presence_of(:person) }
    it { should validate_presence_of(:data_type) }
    it { should validate_presence_of(:value) }
    it do
      should define_enum_for(:data_type)
        .with([:phone, :email, :site])
    end
  end
end
