require 'rails_helper'

RSpec.describe Category, type: :model do
  context 'Associations' do
    it { should have_and_belong_to_many(:people) }
  end

  context 'Validations' do
    it { should validate_presence_of(:name) }
  end
end
