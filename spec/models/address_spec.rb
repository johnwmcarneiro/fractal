require 'rails_helper'

RSpec.describe Address, type: :model do
  context 'Associations' do
    it { should belong_to(:person) }
  end

  context 'Validations' do
    it { should validate_presence_of(:person) }
    it { should validate_presence_of(:street) }
  end
end
