require 'rails_helper'

RSpec.describe Person, type: :model do
  context 'Associations' do
    it { should belong_to(:user) }
    it { should have_and_belong_to_many(:categories) }
    it { should have_many(:contact_data).dependent(:destroy) }
    it do
      should accept_nested_attributes_for(:contact_data)
        .allow_destroy(true)
    end
    it { should have_many(:addresses).dependent(:destroy) }
    it do
      should accept_nested_attributes_for(:addresses)
        .allow_destroy(true)
    end
  end
  context 'Validations' do
    it { should validate_presence_of(:user_id) }
    it { should validate_presence_of(:user) }
    it { should validate_presence_of(:name) }
  end
end
