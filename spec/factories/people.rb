require 'ffaker'

FactoryBot.define do
  factory :person do
    name FFaker::Name.name
    company FFaker::Company.name
    title FFaker::Job.title
    born_at FFaker::Time.date
    association :user, factory: :user
  end
end
