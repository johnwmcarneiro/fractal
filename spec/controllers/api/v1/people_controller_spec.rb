require 'rails_helper'
require 'ffaker'

shared_examples 'check_person' do
  it { expect(json).to include('person') }
  it { expect(json['person']).to include('id', 'name', 'company', 'title',
                                         'born_at', 'created_at', 'categories',
                                         'contact_data', 'addresses') }
end

RSpec.describe Api::V1::PeopleController, type: :controller do
  let!(:user) { create(:user) }
  let!(:person) { create(:person, user: user) }
  let(:person_id) { person.id }
  let(:valid_attributes) do
    { name: FFaker::Name.name }
  end
  let(:valid_attrs_with_auxs) do
    {
      name: FFaker::Name.name,
      company: FFaker::Company.name,
      title: FFaker::Job.title,
      born_at: FFaker::Time.date,
      category_ids: [1, 3, 5],
      contact_data_attributes: [
        { data_type: 'phone', value: FFaker::PhoneNumberBR.phone_number },
        { data_type: 'email', value: FFaker::Internet.email }
      ],
      addresses_attributes: [
        { 
          street: FFaker::AddressBR.street_name,
          number: FFaker::AddressBR.building_number,
          complement: FFaker::AddressBR.secondary_address,
          neighborhood: FFaker::AddressBR.neighborhood,
          city: FFaker::AddressBR.city,
          state: FFaker::AddressBR.state_abbr,
          zipcode: FFaker::AddressBR.zip_code
        }
      ]
    }
  end
  let(:invalid_attributes) { { name: nil } }

  before(:each) do
    request.headers.merge!('devise.mapping' => Devise.mappings[:user],
                           'X-User-Email' => user.email,
                           'X-User-Token' => user.authentication_token)
  end

  describe 'need sign in' do
    before do
      request.headers.merge! :'X-User-Email' => nil, :'X-User-Token' => nil
      get :index, format: :json
    end

    it { expect(json).to be_kind_of(Hash).and include('error') }
    it { expect(json['error']).to match(/You need to sign/) }
  end

  describe 'GET /people.json' do
    before { get :index, format: :json }

    it 'returns status code 200' do
      expect(response).to be_success
    end

    it { expect(json).to include('people') }
    it { expect(json).to include('meta') }

    it 'return one row' do
      expect(json['people'].size).to eq(1)
    end

    it 'include attributes in row' do
      expect(json['people'][0]).to include('id', 'name', 'company', 'title',
                                           'born_at', 'created_at', 'categories',
                                           'contact_data', 'addresses')
    end
  end

  describe 'GET /people/:id.json' do
    before { get :show, format: :json, params: { id: person_id } }

    context 'when person exists' do
      it { expect(response).to be_success }
      include_examples 'check_person'
      it { expect(json['person']['id']).to eq(person.id) }
    end

    context 'when person does not exists' do
      let(:person_id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Person/)
      end
    end
  end

  describe 'POST /people.json' do
    context 'valid params' do
      before { post :create, format: :json, params: valid_attributes }
      it { expect(response).to be_success }
      include_examples 'check_person'
      it { expect(json['person']['id']).not_to be_nil }
      it { expect(json['person']['name']).to eq(valid_attributes[:name]) }
    end

    context 'with auxiliaries' do
      before { post :create, format: :json, params: valid_attrs_with_auxs }

      it { expect(response).to be_success }
      include_examples 'check_person'
      it { expect(json['person']['id']).not_to be_nil }
      it { expect(json['person']['name']).to eq(valid_attrs_with_auxs[:name]) }
    end

    context 'invalid params' do
      before { post :create, format: :json, params: invalid_attributes }
      it { expect(response).to have_http_status(422) }
      it { expect(response.body).to match(/Name can't be blank/) }
    end
  end

  describe 'PUT /people/:id.json' do
    let(:valid_attributes) { { name: 'Change person name', id: person.id } }

    context 'when the record exists' do
      before { put :update, format: :json, params: valid_attributes }
      it { expect(response).to have_http_status(200) }
      include_examples 'check_person'
      it { expect(json['person']['id']).not_to be_nil }
      it 'should change name' do
        expect(json['person']['name']).to eq(valid_attributes[:name])
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'returns status code 200' do
      delete :destroy, format: :json, params: { id: person.id }
      expect(response).to have_http_status(204)
    end
  end
end
