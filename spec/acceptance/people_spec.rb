require 'rails_helper'
require 'rspec_api_documentation/dsl'
require 'ffaker'

def create_person(user, name = FFaker::Name.name)
  p = Person.create(name: name,
                    company: FFaker::Company.name,
                    title: FFaker::Job.title,
                    born_at: FFaker::Time.date,
                    user_id: user.id)

  ContactDatum.create(person_id: p.id,
                      data_type: 'phone',
                      value: FFaker::PhoneNumberBR.phone_number)

  ContactDatum.create(person_id: p.id,
                      data_type: 'email',
                      value: FFaker::Internet.email)

  Category.order('RANDOM()').limit(3).each do |category|
    sql = "INSERT INTO categories_people (category_id, person_id) VALUES (#{category.id}, #{p.id})"
    ActiveRecord::Base.connection.execute(sql)
  end

  Address.create(person_id: p.id,
                 street: FFaker::AddressBR.street_name,
                 number: FFaker::AddressBR.building_number,
                 complement: FFaker::AddressBR.secondary_address,
                 neighborhood: FFaker::AddressBR.neighborhood,
                 city: FFaker::AddressBR.city,
                 state: FFaker::AddressBR.state_abbr,
                 zipcode: FFaker::AddressBR.zip_code)
    p
end

resource 'People' do
  let(:user) { create(:user) }
  let(:auth_email) { user.email }
  let(:auth_token) { user.authentication_token }
  let(:person) { create_person(user) }

  header 'Accept', 'application/vnd.api+json'
  header 'X-User-Email', :auth_email
  header 'X-User-Token', :auth_token

  get '/api/v1/people' do
    parameter :page, 'Choose the page number'
    parameter :q, 'Perform textual search'

    example 'Listing people' do
      explanation 'Return people records. They can be filtered and will be paginated.'

      person

      do_request

      expect(status).to eq(200)
    end

    example 'Search people include owner\'s category' do
      20.times { create_person(user) }
      Person.reindex

      do_request q: 'owner'

      expect(status).to eq(200)
    end
  end

  get '/api/v1/people/:id' do
    let(:id) { person.id }

    example 'Show person\'s info' do
      expect(path).to eq("/api/v1/people/#{person.id}")

      do_request

      expect(status).to eq(200)
    end
  end

  post '/api/v1/people' do
    parameter :name, 'Person name'
    parameter :email, 'Person email'
    parameter :company, 'Person company'
    parameter :title, 'Person title'
    parameter :born_at, 'Person date of birth'
    parameter :category_ids, 'Category ids'
    parameter :contact_data, 'Contact Datum collection'
    parameter :addresses_attributes, 'Address collection'

    example 'Create person' do
      do_request(name: FFaker::Name.name,
                 company: FFaker::Company.name,
                 title: FFaker::Job.title,
                 born_at: FFaker::Time.date,
                 category_ids: [1, 2, 3],
                 contact_data_attributes: [
                   { data_type: 'phone',
                     value: FFaker::PhoneNumberBR.phone_number },
                   { data_type: 'email',
                     value: FFaker::Internet.email }
                 ],
                 addresses_attributes: [
                   { street: FFaker::AddressBR.street_name,
                     number: FFaker::AddressBR.building_number,
                     complement: FFaker::AddressBR.secondary_address,
                     neighborhood: FFaker::AddressBR.neighborhood,
                     city: FFaker::AddressBR.city,
                     state: FFaker::AddressBR.state_abbr,
                     zipcode: FFaker::AddressBR.zip_code }
                 ])

      expect(status).to eq(201)
    end
  end

  put '/api/v1/people/:id' do
    let(:id) { person.id }

    parameter :name, 'Person name'
    parameter :email, 'Person email'
    parameter :company, 'Person company'
    parameter :title, 'Person title'
    parameter :born_at, 'Person date of birth'
    parameter :category_ids, 'Category ids'
    parameter :contact_data, 'Contact Datum collection'
    parameter :addresses_attributes, 'Address collection'

    example 'Update person' do
      do_request(name: FFaker::Name.name,
                 company: FFaker::Company.name,
                 title: FFaker::Job.title,
                 born_at: FFaker::Time.date,
                 category_ids: [2, 5],
                 contact_data_attributes: [
                   { id: person.contact_data.first.id,
                     data_type: 'phone',
                     value: FFaker::PhoneNumberBR.phone_number },
                   { id: person.contact_data.second.id,
                     data_type: 'email',
                     value: FFaker::Internet.email }
                 ],
                 addresses_attributes: [
                   { id: person.addresses.first.id,
                     street: FFaker::AddressBR.street_name,
                     number: FFaker::AddressBR.building_number,
                     complement: FFaker::AddressBR.secondary_address,
                     neighborhood: FFaker::AddressBR.neighborhood,
                     city: FFaker::AddressBR.city,
                     state: FFaker::AddressBR.state_abbr,
                     zipcode: FFaker::AddressBR.zip_code }
                 ])

      expect(status).to eq(200)
    end
  end

  delete '/api/v1/people/:id' do
    let(:id) { person.id }

    example 'Delete person' do
      do_request

      expect(status).to eq(204)
    end
  end
end
