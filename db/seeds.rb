unless User.exists? email: 'test@example.com'
  User.create email: 'test@example.com', password: '123456', password_confirmation: '123456'
end

Category.init_categories

require 'ffaker'
100.times do
  user = User.first
  person = Person.create(name: FFaker::Name.name,
                         company: FFaker::Company.name,
                         title: FFaker::Job.title,
                         born_at: FFaker::Time.date,
                         user_id: user.id)

  person.contact_data.create(data_type: 'phone',
                             value: FFaker::PhoneNumberBR.phone_number)
  person.contact_data.create(data_type: 'email',
                             value: FFaker::Internet.email)

  Category.order('RANDOM()').limit(3).each do |category|
    person.categories << category
  end

  person.addresses.create(street: FFaker::AddressBR.street_name,
                          number: FFaker::AddressBR.building_number,
                          complement: FFaker::AddressBR.secondary_address,
                          neighborhood: FFaker::AddressBR.neighborhood,
                          city: FFaker::AddressBR.city,
                          state: FFaker::AddressBR.state_abbr,
                          zipcode: FFaker::AddressBR.zip_code)
end

Person.reindex
