class CreateContactData < ActiveRecord::Migration[5.1]
  def change
    create_table :contact_data do |t|
      t.integer :data_type
      t.string :value
      t.references :person, foreign_key: true

      t.timestamps
    end
  end
end
