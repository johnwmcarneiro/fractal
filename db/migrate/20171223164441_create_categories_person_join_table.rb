class CreateCategoriesPersonJoinTable < ActiveRecord::Migration[5.1]
  def change
    create_join_table :categories, :people do |t|
      t.index :category_id
      t.index :person_id
    end
  end
end
