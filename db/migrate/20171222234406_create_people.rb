class CreatePeople < ActiveRecord::Migration[5.1]
  def change
    create_table :people do |t|
      t.string :name
      t.string :company
      t.string :title
      t.date :born_at
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
